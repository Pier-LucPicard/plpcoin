
const crypto = require('node:crypto');

const express = require('express')
const app = express()
const port = 3000

class BlockChain {

    constructor() {
        this.chain = []
        this.createBlock(1, "0")
    }

    createBlock(proof, previous_hash) {
       const block = {
            index: this.chain.length + 1,
            timestamp: new Date().toDateString(),
            proof,
            previous_hash,
        };
        this.chain.push(block);
        return block;
    }

    getPreviousBlock() {
        return this.chain[this.chain.length -1]
    }

    proofOfWork(previousProof) {
        let newProof = 1;
        let checkProof = false;

        while(!checkProof){
            let hashOperation = crypto.createHash('sha256').update(`${newProof*newProof - previousProof * previousProof}`).digest('hex')
            if(hashOperation.substring(0,4) === '0000') {
                checkProof = true
            }else{
                newProof++;
            }
        }
        return newProof
    }

    hash(block) {
        const encodedBlock =  crypto.createHash('sha256').update(JSON.stringify(block)).digest('hex')
        return encodedBlock;
    }

    isChainValid(chain){
        let previousBlock = chain[0];
        let block_index = 1;
        while(block_index < chain.length){
            const currentBlock = chain[block_index];
            if(currentBlock.previous_hash !== this.hash(previousBlock)){
                return false
            }

            const previousProof = previousBlock.proof;
            const proof = currentBlock.proof
            const hashOperation = crypto.createHash('sha256').update(`${proof*proof - previousProof * previousProof}`).digest('hex')
            if(hashOperation.substring(0,4) !== '0000' ) {
                return false
            }

            previousBlock = currentBlock;
            block_index++;
        }
        return true;
    }

}

const blockchain = new BlockChain()


app.get('/mine-block', (req, res) => {
    const previousBlock = blockchain.getPreviousBlock()
    const newProof = blockchain.proofOfWork(previousBlock.proof)
    const previousHash = blockchain.hash(previousBlock)

    res.status(200).send({
        message: 'Congratulation you just mined a block',
        ...blockchain.createBlock(newProof, previousHash)
    })
})


app.get('/get-chain', (req, res) => {
    
    res.status(200).send({
        chain: JSON.stringify(blockchain.chain),
        length: blockchain.chain.length
    })
})

app.get('/is-valid', (req, res) => {
    const isValid = blockchain.isChainValid(blockchain.chain)
    let message = ''
    if(isValid){
        message= 'All is good with the block chain'
    }else{
        message =  'Houston, we have a problem.  The blockchain is not valid.'
    }
    res.status(200).send({ message })

})



app.get('/', (req, res) => {
  res.send('Hello World!')
})


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})