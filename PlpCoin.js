const {nanoid} = require('nanoid')

const crypto = require('node:crypto');

const express = require('express')
const bodyParser = require('body-parser')

const axios = require('axios');

const path = require('node:path');
const url = require('node:url');

const app = express()
app.use(bodyParser.json())
const port = process.argv[2] || 3000
const user = process.argv[3] || 'Picard'

console.log(process.argv)

class BlockChain {

    constructor() {
        this.chain = []
        this.transactionPool = []
        this.createBlock(1, "0")
        this.nodes = new Set()
    }

    createBlock(proof, previous_hash) {
       const block = {
            index: this.chain.length + 1,
            timestamp: new Date().toDateString(),
            proof,
            previous_hash,
            transactions: this.transactionPool
        };
        this.transactionPool = [];
        this.chain.push(block);
        return block;
    }

    getPreviousBlock() {
        return this.chain[this.chain.length -1]
    }

    proofOfWork(previousProof) {
        let newProof = 1;
        let checkProof = false;

        while(!checkProof){
            let hashOperation = crypto.createHash('sha256').update(`${newProof*newProof - previousProof * previousProof}`).digest('hex')
            if(hashOperation.substring(0,4) === '0000') {
                checkProof = true
            }else{
                newProof++;
            }
        }
        return newProof
    }

    hash(block) {
        const encodedBlock =  crypto.createHash('sha256').update(JSON.stringify(block)).digest('hex')
        return encodedBlock;
    }

    isChainValid(chain){
        let previousBlock = chain[0];
        let block_index = 1;
        while(block_index < chain.length){
            const currentBlock = chain[block_index];
            if(currentBlock.previous_hash !== this.hash(previousBlock)){
                return false
            }

            const previousProof = previousBlock.proof;
            const proof = currentBlock.proof
            const hashOperation = crypto.createHash('sha256').update(`${proof*proof - previousProof * previousProof}`).digest('hex')
            if(hashOperation.substring(0,4) !== '0000' ) {
                return false
            }

            previousBlock = currentBlock;
            block_index++;
        }
        return true;
    }

    addTransaction(sender, receiver, amount) {
        this.transactionPool.push({
            sender,
            receiver,
            amount
        })
        return this.getPreviousBlock().index + 1
    }

    addNode(address) {
        this.nodes.add(url.parse(address).host)
    }

    async replaceChain(){

        let longestChain = null;
        let maxLength = this.chain.length
        for (const node of this.nodes) {
            console.log(node)
            try{
                const response = await axios.get(`http://${node}/get-chain`);
                const {length, chain} = response.data
                
                if(length > maxLength && this.isChainValid(chain)){
                    maxLength = length;
                    longestChain = chain;
                }
            }catch(err){
                console.log(err)
            }
          
        }
        if(!!longestChain){
            this.chain = longestChain
            return true;
        }

        return false
    }

}

nodeAddress = nanoid(32).replace('-', '');// 'localhost:3000';
console.log(nodeAddress)

const blockchain = new BlockChain()

app.post('/add-transaction', (req, res) => {
    const transactionKey = ['sender', 'receiver', 'amount'];
    for (const key of transactionKey) {
        if(!Object.keys(req.body).includes(key)){
            res.status(400).send({ message: 'Invalid Request' })
            return ''
        }
    }
    const index = blockchain.addTransaction(req.body.sender, req.body.receiver, req.body.amount)
    res.status(201).send({
        message: `The transaction will be added to the block with index ${index}`
    })
})

app.post('/connect-node', (req, res) => {
    const payload = req.body;
    if(!Object.keys(payload).includes('nodes')){
        res.status(400).send({ message: 'Invalid Request' })
        return ''
    }
    for (const nodeAddress of payload.nodes) {
        blockchain.addNode(nodeAddress)
    }

    res.status(201).send({
        message: `All the nodes are connected. The PLP now contains the following nodes`,
        total_node: [...blockchain.nodes.values()]
    })
})

app.get('/mine-block', (req, res) => {
    const previousBlock = blockchain.getPreviousBlock()
    const newProof = blockchain.proofOfWork(previousBlock.proof)
    const previousHash = blockchain.hash(previousBlock)

    blockchain.addTransaction(nodeAddress, user, 1000)

    res.status(200).send({
        message: 'Congratulation you just mined a block',
        ...blockchain.createBlock(newProof, previousHash)
    })
})

app.get('/replace-chain', async (req, res) => {
    const isValid = await blockchain.replaceChain()

    if(isValid){
        res.status(200).send({ message: 'The chain has been replace', new_chain: blockchain.chain })
        return
    }

    res.status(200).send({ message: 'Everything is fine, nothing changed', actual_chain: blockchain.chain })   

})


app.get('/get-chain', (req, res) => {
    
    res.status(200).send({
        chain: blockchain.chain,
        length: blockchain.chain.length
    })
})

app.get('/is-valid', (req, res) => {
    const isValid = blockchain.isChainValid(blockchain.chain)
    let message = ''
    if(isValid){
        message= 'All is good with the block chain'
    }else{
        message =  'Houston, we have a problem.  The blockchain is not valid.'
    }
    res.status(200).send({ message })

})





app.get('/', (req, res) => {
  res.send('Hello World!')
})


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})